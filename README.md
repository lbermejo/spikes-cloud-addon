# Spikes addon tutorial
Using atlassian-connect-express
### Dev instance
  [https:lab-tutorial1.atlassian.net](https:lab-tutorial1.atlassian.net)

### Preparation
  * Update **credentials.json** with your host URL and credentials.
  * Run in console: `npm install`

### How to run as dev loop (watch)
    nodemon app.js
  or

    npx nodemon

  (`npx` will run the `nodemon` from node_modules, and then `nodemon` will run the module indicated in **package.json** > `"main"`. )
  ##### Autorestart
  * atlassian-connect will automatically reload for changes in hbs files (views)
  * nodemon will automatically reload for changes in javascript files
  ##### Manual restart
  * In the console running nodemon, type `rs` if you need to restart.